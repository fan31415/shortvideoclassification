import pickle
import os
import sklearn
# import xgboost


from config import *
from data_helper import *

from sklearn.svm import SVC

from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.decomposition import PCA



import operator


# extract_files()
# generate_data_file()
# exit()
# extract_feature()
# exit()


# load_data_index(True)
# exit()
data = load_data(True)
print(data.head())
# exit()

t_data = load_data(False)

t_index = load_data_index(False)
t_counts = t_index[4]

t_video_names = list(set(t_data['video_name']))

t_X = t_data.drop("label", axis = 1)
t_X = t_X.drop("video_name", axis = 1)




video_names = list(set(data['video_name']))

# validation_portion = 20

# test_name = video_names[:len(video_names)//validation_portion]
# train_name = video_names[len(video_names)//validation_portion:]
train_name = video_names

# test_data = data[data["video_name"].isin(test_name)]
train_data = data

train_y =  train_data["label"]
# test_y = test_data["label"]

train_X = train_data.drop("label", axis = 1)
train_X = train_X.drop("video_name", axis = 1)

# test_X = test_data.drop("label", axis = 1)
# test_X = test_X.drop("video_name", axis = 1)


clfs = []
bagging_count = 1
test_limit = 100
pca = PCA(n_components=80)
for i in range(bagging_count):
    # choice = random.sample(list(range(len(train_X))), len(train_X) // 1)

    # X = train_X.iloc[choice]
    # y = train_y.iloc[choice]
    X = train_X
    y = train_y

    X = pca.fit_transform(X.values)

    # for item in y:
    #     if item == 11:
    #         print(item)
    # exit()


    clf = LogisticRegression(multi_class='multinomial', solver="lbfgs", class_weight="balanced").fit(X, y)
    # clf = SVC(gamma="auto", class_weight="balanced")
    # clf.fit(X, y)

    clfs.append(clf)

    # test_X = pca.transform(test_X)
    # y_pred = clf.predict(test_X[:test_limit])
    # print(sklearn.metrics.classification_report(test_y[:test_limit], y_pred))


pred_list = []





temp_results = []

category_count = 15

big_votes = {}

# name = test_name
name = t_video_names
print(len(t_X))
# exit()
for bag_idx in range(bagging_count):
    temp_result = []
    t_X = pca.transform(t_X)
    preds = clfs[bag_idx].predict(t_X)

    preds_idx = 0

    for name_idx in range(len(name)):

        votes = {}

        # for i in range(category_count):
        #     votes[i] = 0
        # print(name_idx)
        # print(preds_idx)
        # print(t_counts[name_idx])
        # print(len(preds))
        for j in range(t_counts[name_idx]):
        # for j in range(5):
            if preds_idx + j >= len(preds):
                break
            # votes[preds[preds_idx + j]] += 1

            key = preds[preds_idx + j]
            # print(key)
            if key in votes.keys():
                votes[key] += 1
            else:
                votes[key] = 1
        preds_idx += t_counts[name_idx]
        # preds_idx += 5
        # print(votes)
        # if votes != {}:
        vote = max(votes, key=votes.get)
        # print(vote)
        temp_result.append(vote)

    print(temp_result)

    temp_results.append(temp_result)

temp_results = np.array(temp_results)


# print(temp_results)
# print(temp_results[0])
# print(temp_results[0][2])
# exit()
use_determin = False

np_result = []

if use_determin:

    for name_idx in range(len(name)):
        votes = {}
        # for idx in range(category_count):
        #     votes[idx] = 0
        pred = temp_results[0][name_idx]
        np_result.append([str(name[name_idx]) + ".mp4", pred])

    result = pd.DataFrame(np.array(np_result), columns=['file_name', 'label'])
    result.to_csv("submission.csv", index=None)
    print(result)
    print("finished")
    exit()
for name_idx in range(len(name)):
    votes = {}
    # for idx in range(category_count):
    #     votes[idx] = 0

    for i in range(bagging_count):
        key = temp_results[i][name_idx]
        if key in votes.keys():
            votes[key] +=1
        else:
            votes[key] = 1

    vote = max(votes, key=votes.get)
    np_result.append([name[name_idx] + ".mp4", vote])


result = pd.DataFrame(np.array(np_result), columns=['file_name', 'label'])
result.to_csv("submission.csv", index=None)
print(result)

#
#
# print(pred_list)
#
# for i in range(bagging_count):
#
#
# result = np.sum(np.array(pred_list), axis=1)/bagging_count
# print(result)
# exit()
# test_data = load_data(True)
# video_names = list(set(test_data['video_name']))








