import glob
import os
import csv
import subprocess
import shlex
import math
import random

import cv2

import pandas as pd

import numpy as np

from keras.applications.inception_v3 import InceptionV3
# import os
# os.environ['HDF5_DISABLE_VERSION_CHECK']= '1'
import keras

from keras.preprocessing.image import ImageDataGenerator
from keras.models import Model
from keras.layers import Dense, GlobalAveragePooling2D

tags = {}
with open("train_tag.txt", "r") as f:
    for line in f:
        parts = line.split(',')

        tags[parts[0]] = parts[1][:-1]
        # print(tags[parts[0]])

isTest = False

def extract_files():
    """After we have all of our videos split between train and test, and
    all nested within folders representing their classes, we need to
    make a data file that we can reference when training our RNN(s).
    This will let us keep track of image sequences and other parts
    of the training process.
    We'll first need to extract images from each of the videos. We'll
    need to record the following data in the file:
    [train|test], class, filename, nb frames
    Extracting can be done with ffmpeg:
    `ffmpeg -i video.mpg image-%04d.jpg`
    """
    data_file = []

    folders = ['train', 'test']


    for folder in folders:
        class_files = glob.glob(os.path.join(folder, '*.mp4'))
        random.shuffle(class_files)

        for video_path in class_files:
            # Get the parts of the file.
            video_parts = get_video_parts(video_path, tags)

            train_or_test, classname, filename_no_ext, filename = video_parts

            # Only extract if we haven't done it yet. Otherwise, just get
            # the info.
            if not check_already_extracted(video_parts):
                # preprocessing

                # Now extract it.
                src = os.path.join(train_or_test, filename)


                raw_duration = subprocess.check_output(['ffprobe', '-v', 'error', '-show_entries',
                                 'format=duration', '-of', 'default=noprint_wrappers=1:nokey=1', src])

                duration = float(raw_duration[:-2])



                # cut off the logo tail
                duration = duration - 1.1

                rate = 5/duration



                nb_frames = subprocess.check_output(['ffprobe', '-v', 'error', '-count_frames', '-select_streams', 'v:0', '-show_entries',
                             'stream=nb_frames', '-of', 'default=nokey=1:noprint_wrappers=1', src])

                nb_frames = int(nb_frames[:-1])

                dest = os.path.join("processed", train_or_test, filename_no_ext + '-%02d.jpg')



                selectStr = "select='not(mod(n,"
                # selectStr = selectStr + "100))'"
                selectStr = selectStr + str(math.ceil(nb_frames/6)) + "))'"

                # subprocess.call(['ffmpeg', '-i', src, '-vf', selectStr,  dest])

                # subprocess.call(["ffmpeg", '-ss', '0.1', '-t', str(duration), "-i", src, "-r", str(rate), dest])


                # dest = os.path.join(train_or_test,
                #                     filename_no_ext + '-%04d.jpg')


                # subprocess.call(['ffmpeg', '-ss', '0', '-i', src, '-t', str(duration - 1.1), '-c', 'copy', dest])


                subprocess.call(["ffmpeg","-v", "error", "-i", src,'-ss', '0.1', '-t', str(duration), "-vf", selectStr, "-vsync", "0", dest])
                # call(["ffmpeg", "-i", src, dest])
            # Now get how many frames it is.
            nb_frames = get_nb_frames_for_video(video_parts)

            data_file.append([train_or_test, classname, filename_no_ext, nb_frames])

            print("Generated %d frames for %s" % (nb_frames, filename_no_ext))

    with open('data_file.csv', 'w') as fout:
        writer = csv.writer(fout)
        writer.writerows(data_file)

    print("Extracted and wrote %d video files." % (len(data_file)))

def get_video_parts(video_path, tags):
    """Given a full path to a video, return its parts."""
    parts = video_path.split(os.path.sep)
    filename = parts[1]
    filename_no_ext = filename.split('.')[0]
    train_or_test = parts[0]
    classname = tags.get(filename, "-1")

    return train_or_test, classname, filename_no_ext, filename

def check_already_extracted(video_parts):
    """Check to see if we created the -0001 frame of this file."""
    train_or_test, classname, filename_no_ext, _ = video_parts
    return bool(os.path.exists(os.path.join("processed", train_or_test,
                               filename_no_ext + '-0001.jpg')))

def get_nb_frames_for_video(video_parts):
    """Given video parts of an (assumed) already extracted video, return
    the number of frames that were extracted."""
    train_or_test, classname, filename_no_ext, _ = video_parts
    generated_files = glob.glob(os.path.join("processed", train_or_test,
                                filename_no_ext + '*.jpg'))
    return len(generated_files)





def extract_feature():
    base_model = InceptionV3(weights='imagenet', include_top=False, pooling="avg")
    prefix = "processed"
    folders = [os.path.join(prefix, 'train'), os.path.join(prefix, 'test')]

    imgs = []
    labels = []
    video_names = []

    features = []
    for folder in folders:
        files = glob.glob(os.path.join(folder, '*.jpg'))
        files.sort()
        # print(files)
        if isTest:
            take_files = files[:66]
        else:
            take_files = files[:]

        total_batch = 2000
        for batch_idx in range(0, len(take_files), total_batch):
            print(batch_idx)
            if isTest:
                train_or_test = folder.split('/')[-1]
            else:
                train_or_test = folder.split('\\')[-1]
            subfiles = files[batch_idx:batch_idx + total_batch]
            imgs = []
            labels= []
            video_names= []
            features = []

            for file in subfiles:
                image_parts = get_image_parts(file, tags)

                train_or_test, classname, index, video_name = image_parts
                image = cv2.imread(file)
                image = cv2.resize(image, (299, 299))
                image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                # cv2.imwrite(file, image)
                # continue
                imgs.append(image)
                labels.append(classname)
                video_names.append(video_name)

            images, categories = normalise_images(imgs, labels)
            # batch_image = split_padded(images, 500)
            last_idx = -1
            print(len(images))
            batch_count = 500
            for idx in range(0, len(images), batch_count):
                feature = base_model.predict(images[idx:idx + batch_count])
                features.append(feature)
                # idx += batch_count
                print(idx)

            features = np.array(features)
            # features = np.squeeze(features, 2)
            # print(np.shape(features))
            features = np.vstack(features)

            # features = keras.layers.GlobalMaxPooling2D(outputs)

            # print(features[0])
            # print(np.shape(features))
            # print(np.shape(features[0]))
            # name = folder.split('/')[-1]
            feature_pd = pd.DataFrame(features)
            # feature_pd.to_csv(name + "_features.csv")
            label_pd = pd.DataFrame(labels, columns=["label"])
            # label_pd.to_csv(name + "_labels.csv")
            video_name_pd = pd.DataFrame(video_names, columns=["video_name"])
            video_name_pd.to_csv("video_name_" + train_or_test + str(batch_idx) + ".csv")

            data_pd = pd.concat([feature_pd, video_name_pd, label_pd], axis=1)
            data_pd.to_csv("datas_" + train_or_test + str(batch_idx) + ".csv")


def split_padded(a,n):
    padding = (-len(a))%n
    result = np.split(np.concatenate((a,np.zeros(padding))),n)
    result[-1] = result[-1][:-padding]
    return result


def get_image_parts(path, tags):

    """Given a full path to a video, return its parts."""
    parts = path.split(os.path.sep)
    filename = parts[2]
    filename_no_ext = filename.split('.')[0]
    video_name = filename_no_ext.split('-')[0]
    index = filename_no_ext.split('-')[1]

    train_or_test = parts[1]

    class_name = tags.get(video_name+'.mp4', "-1")
    # print(class_name)
    # if class_name == '14':
        # print(14)

    return train_or_test, class_name, index, video_name


def normalise_images(images, labels):
    # Convert to numpy arrays
    images = np.array(images, dtype=np.float32)
    labels = np.array(labels)

    # Normalise the images
    images /= 255

    return images, labels


def load_data(isTrain):
    train_len = 10001
    test_len = 4001
    if isTrain:
        lenth = train_len
    else:
        lenth = test_len
    data_list = []
    for idx in range(0, lenth, 2000):
        print(idx)
        if isTrain:
            filename = "datas_train" + str(idx) + '.csv'
        else:
            filename = "datas_test" + str(idx) + '.csv'
        data = pd.read_csv(filename)
        data_list.append(data)
    datas = pd.concat(data_list, axis=0)
    # print(datas)
    #
    #
    #
    # if isTrain:
    #     datas = datas[datas["label"] != -1]
    # else:
    #     datas = datas[datas["label"] == -1]

    return datas

    # if isTrain:
    #     features = pd.read_csv("train" + "_features.csv")
    #     labels = pd.read_csv("train" + "_labels.csv")
    # else:
    #     features = pd.read_csv("test" + "features.csv")
    #     labels = pd.read_csv("test" + "_labels.csv")
    # return  pd.concat([features, labels], axis=1)


def load_data_index(isTrain):
    datas = pd.read_csv("data_file.csv", header=None)
    if isTrain:
        # print(datas.loc[1, 1])
        # exit()
        datas = datas[datas[1] == "train"]
    else:
        datas = datas[datas[1] != "test"]

    return datas


def generate_data_file():
    prefix = "processed"
    folders = [os.path.join(prefix, 'train'), os.path.join(prefix, 'test')]

    results = []
    for folder in folders:
        files = glob.glob(os.path.join(folder, '*.jpg'))
        files.sort()
        if isTest:
            sub_files = files[:300]
        else:
            sub_files = files

        count = 0

        init_file = sub_files[0]
        image_parts = get_image_parts(init_file, tags)
        train_or_test, classname, index, video_name = image_parts
        last_name = video_name

        last_train_or_test = train_or_test

        last_class_name = classname

        last_count = 5
        # count = 0
        # for file in sub_files:
        #     image_parts = get_image_parts(file, tags)
        #
        #     train_or_test, classname, index, video_name = image_parts
        #
        #
        #     if video_name != last_name:
        #
        #         last_count = count
        #
        #         break
        #     count += 1

        count = 0
        for idx, _ in enumerate(sub_files):
            image_parts = get_image_parts(sub_files[idx], tags)

            train_or_test, class_name, index, video_name = image_parts
            if isTest:
                train_or_test = folder.split('/')[-1]
            else:
                train_or_test = folder.split('\\')[-1]

            # select_str = video_name + "*|wc"
            # select_str = "835407245*|wc"
            # command = "ls -l " + select_str + " -l"
            # print(command)
            # # count = subprocess.check_output(['ls', '-l', select_str, '-l'], shell=True)
            # count = os.system(command)



            # if (last_name == "360170375"):
            #     print(count)
            #     print(last_name)
            #     print(last_count)


            if video_name == last_name:
                count += 1
                continue
            else:

                results.append([last_train_or_test, last_class_name, last_name, count])
                last_name = video_name
                last_class_name = class_name
                last_train_or_test = train_or_test
                count = 1




    result_pd = pd.DataFrame(results)
    result_pd.to_csv("data_file.csv")
